const env = process.env.NODE_ENV; // 'development' or 'production'

const dev = {
    app: {
        port: parseInt(process.env.DEV_APP_PORT) || 3978
    },
    db: {
        uri: process.env.DEV_DB_HOST && process.env.DEV_DB_PORT && process.env.DEV_DB_NAME ? `mongodb://${ process.env.DEV_DB_HOST }:${ process.env.DEV_DB_PORT }/${ process.env.DEV_DB_NAME }` : `mongodb://localhost:27017/qover`
    }
};

const prod = {
    app: {
        port: parseInt(process.env.PROD_APP_PORT) || 3978
    },
    db: {
        uri: ''
    }
};

const config = { 'development': dev, 'production': prod };

module.exports = config[env];
