const path = require('path');
const restify = require('restify');
const registerRoutesPublic = require('./routes/routePublic');
const registerMainRouter = require('./routes/mainRouter');
const corsMiddleware = require('restify-cors-middleware');
const ENV_FILE = path.join(__dirname, '.env');
require ('./connectionDB/mongodb');
require('./routes/scriptAddTest');

// Create HTTP server
let server = restify.createServer();

// CORS: Cross-origin resource sharing with *
const cors = corsMiddleware({
    origins: ['*'],
    allowHeaders: ['*'],
    exposeHeaders: ['*']
});

server.pre(cors.preflight);
server.use(cors.actual);

registerRoutesPublic(server);
registerMainRouter(server);

server.listen(process.env.port || process.env.PORT || 3978, function() {
    console.log('hi listen server');
    console.log(`\n${ server.name } listening to ${ server.url }`);
});

