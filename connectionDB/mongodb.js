//  MongoDB connection

// console.log(JSON.stringify(process.env, null, 2));
require('dotenv').config(); // this loads the defined variables from .env
const mongoose = require('mongoose');
const config = require('../config');
const MONGO_URI = config.db.uri;
console.log('URI :' + MONGO_URI);
// mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true })
mongoose.connect(MONGO_URI, { useNewUrlParser: true });

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('Connected to MongoDB');
});

