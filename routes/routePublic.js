const Router = require('restify-router').Router;
const crypto = require('crypto');
const userClient = require('../models/userClient');
const jwt = require('jsonwebtoken');

let registerRoutesPublic = (server) => {
    const router = new Router();

    router.get('/api/test', testfunction);
    router.post('/api/gettoken', getToken);
    router.applyRoutes(server);

    async function testfunction(req, res, next) {
        try {
            console.log('Public API working');
            res.json(200, 'Public API working');
        } catch (err) {
            console.log(err);
            res.json(500, err);
        }
    }

    async function getToken(req, res, next) {
        const email = req.headers['email'] || null;
        const pw = crypto.createHmac('sha256', req.headers['password'])
            .update('i love cops')
            .digest('hex');

        userClient.find({ email: email }, (err, userClients) => {
            if (!err) {
                const userClient = userClients[0];
                // console.log(userClient);
                if (userClient && userClient['password'] === pw) {
                    let user = { email: email};
                    const token = jwt.sign({ email: email },
                        'Secret-Key',
                        { expiresIn: 60 * 60 * 5 }); // 5 hours
                    res.json({ success: true, message: 'logged in', token: token, user: user });
                } else {
                    res.json({ success: false, message: 'Wrong Password' });
                }
            } else {
                res.json({ success: false, message: 'Wrong email' });
            }
        });
    }
};

module.exports = registerRoutesPublic;
