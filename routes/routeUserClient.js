const Router = require("restify-router").Router;
const UserClient = require("../models/userClient");
const crypto = require("crypto");

const router = new Router();

router.get("/:idUser", getUser);
router.post("/create", createUser);
router.get("/logout", logOut);

async function logOut(res, res, next){
    if (req.session) {
        // delete session object
        req.session.destroy(function(err) {
          if(err) {
            return next(err);
          } else {
            return res.redirect('/');
          }
        });
      }
}


async function getUser(req, res, next) {
  try {
    const user = await UserClient.findById(req.params.idUser);
    res.json(user);
  } catch (err) {
    // console.log(err);
    res.json(500, "error");
  }
}

async function createUser(req, res, next) {
  try {
    const email = req.body.email;
    const pw = crypto
      .createHmac("sha256", req.body.password)
      .update("i love cops")
      .digest("hex");
    const doc = {
      email: email,
      password: pw
    };
    const userClient = new UserClient(doc);
    const newUserClient = await userClient.save();
    res.json(newUserClient);
  } catch (err) {
    // console.log(err);
    res.json(500, "error");
  }
}

module.exports = router;
