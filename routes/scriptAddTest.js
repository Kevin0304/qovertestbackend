//Insert default user in database if not already exists

const UserClient = require("../models/userClient");

UserClient.find({ email: "hello@qover.com" }).countDocuments(function(err, count) {
  if (err) {
    return;
  }
  if (count <= 0) {
    const crypto = require("crypto");
    const pw = crypto
      .createHmac("sha256", "Ninja")
      .update("i love cops")
      .digest("hex");
    const doc = { email: "hello@qover.com", password: pw };
    const userClient = new UserClient(doc);
    userClient.save();
  }
});
