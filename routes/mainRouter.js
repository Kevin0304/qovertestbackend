const Router = require('restify-router').Router;
const routeUserClient = require('../routes/routeUserClient');
const createError = require('http-errors');
const jwt = require('jsonwebtoken');

let registerMainRouter = (server) => {
    let router = new Router();

    // Middleware, check if there is a token and check it. Write the data from the token in req.decoded
    router.use(function(req, res, next) {
        let token = req.headers['authorization'] || req.headers['x-access-token'] || req.body.token;
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, 'Secret-Key', function(err, decoded) {
                if (err) {
                    return next(createError(401, 'Failed to authenticate token.'));
                } else {
                    req.decoded = decoded;
                    return next();
                }
            });
        } else {
            return next(createError(401, 'No token provided.'));
        }
    });
    router.add('/api/userclient', routeUserClient);
    router.applyRoutes(server);
};

module.exports = registerMainRouter;
